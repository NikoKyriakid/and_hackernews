const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');

//app.use(bodyParser.json());

app.use('/', express.static(path.resolve(__dirname, 'dist/')));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/dist/index.html');
});

app.listen(4000, () => console.log('Mock server listening on port 4000!'));
