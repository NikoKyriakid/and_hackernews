# AND_HackerNews

```bash
# 1.
git clone https://NikoKyriakid@bitbucket.org/NikoKyriakid/and_hackernews.git
# 2.
cd and_hackernews
# 3.
npm install
# 4. 
npm start
```

2. Visit [http://localhost:4000](http://localhost:4000)

3. In the begging you are going to see a loading animation, please wait the client downloads all the topstories

4. After the loading is done you gonna see the resulting page with the stories and comments. Please click on the comment counter to display the comments.
