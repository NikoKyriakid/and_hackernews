import Story from './Story';

export default class Client {
    constructor(options) {
        let count = 0;
        if (!(options && options.stories)) {
            console.error('No stories were passed to the Client.');
            return null;
        }

        this.stories = options.stories.map(story => {
            count++;
            return new Story(story, count);
        });

        this.className = 'stories';
    }

    getStories() {
        return this.stories;
    }

    getStory(id) {
        const filtered = this.stories.filter(story => {
            return story.id.toString() === id;
        });

        return filtered && filtered[0];
    }

    render(inject=true) {
        const array_template = this.stories.map(story => {
            return story.render();
        });

        const html = array_template.join('');

        if (inject) {
            this.append(html).addHandlers();
        } else {
            return html;
        }
    }

    append(content) {
        const elChild = document.createElement('div');
        elChild.classList.add(this.className);
        elChild.innerHTML = content;
        document.querySelector('#app').appendChild(elChild);
        return this;
    }

    addHandlers() {
        document.querySelector('#app').addEventListener("click", e => {
            if (e.target.classList.contains('comment-counter')) {
                const storyId = e.target.parentElement.parentElement.id;
                this.getStory(storyId).toggle();
            }
        },false);

        return this;
    }

}
