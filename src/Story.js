import moment from 'moment';
import Fetch from './Fetch';

export default class Story {
    constructor(options, number) {
        this.id        = options.id;
        this.number    = number || 0;
        this.title     = options.title;
        this.type      = options.type;
        this.by        = options.by;
        this.score     = options.score;
        this.time      = options.time;
        this.age       = moment(options.time*1000).fromNow();
        this.kids       = options.kids || [];
        this.count     = this.kids.length;
        this.url       = options.url;

        const matches  = /^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/im.exec(options.url);
        this.source    = matches[0];
    }

    getComments() {
        const tasks = this.kids.map(kid => {
            let commentURL = `https://hacker-news.firebaseio.com/v0/item/${kid}.json`;
            return Fetch(commentURL).then(function(comment){
                return comment;
            })
        });

        return Promise.all(tasks);
    }

    render(data) {
        return `<div id='${this.id}' class='story'>${this.renderTitle() + this.renderExtras()}</div>`;
    }

    renderTitle() {
        return `<div class='up'>
                <span class='number'>${this.number}.</span>
                <span class='arrow'></span>
                <span class='title'>${this.title}</span>
                <span class='source'>(${this.source})</span>
                </div>`;
    }

    renderExtras() {
        return [
            `<div class='bottom'>`,
                `<span class='number pseudo-number'>${this.number}.</span>`,
                `<span class='score'>${this.score}</span>`,
                ` by <span class='user'>${this.by}</span>`,
                ` <span class='age'>${this.age}</span> | `,
                ` <span class='comment-counter'>${this.count} comment${this.count > 1 ? 's' : ''}</span>`,
            `</div>`
          ].join('');
    }

    renderComments() {
        return this.getComments().then(comments => {
            return comments.map(comment => {
                return [
                    `<div class='comment'>`,
                        `<div class='comment-extras'><span class='arrow'></span> ${comment.by} ${moment(comment.time*1000).fromNow()} [-]</div>`,
                        `<div class='comment-main'>${comment.text}</div>`,
                    `</div>`
                ].join('');
            }).join('');
        });
    }

    toggle() {
        const storyDiv = document.getElementById(this.id);
        const arrowDiv = storyDiv.querySelector('.arrow');

        arrowDiv.classList.toggle('flipped');
        const open = arrowDiv.classList.contains('flipped');    // means comments are open

        if (open) {
            this.renderComments().then(html => {
                const elChild = document.createElement('div');
                elChild.classList.add('comments');
                elChild.innerHTML = html;
                storyDiv.appendChild(elChild);
            });
        } else {
            storyDiv.querySelector('.comments').remove();
        }
        
    }
}



/*


{
  "by": "Varcht",
  "descendants": 2,
  "id": 16777551,
  "kids": [
    16778888,
    16778894
  ],
  "score": 11,
  "time": 1523049612,
  "title": "Backpage.com taken offline as part of an enforcement action, officials say",
  "type": "story",
  "url": "https://www.washingtonpost.com/local/public-safety/backpagecom-taken-offline-as-part-of-an-enforcement-action-federal-officials-say/2018/04/06/47bb93de-39d0-11e8-9c0a-85d477d9a226_story.html"
}

*/