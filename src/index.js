
import 'normalize.css';
import './css/main.css';
import './css/loader.css';

import Client from './Client';
import Fetch from './Fetch';

document.addEventListener('DOMContentLoaded', () => {
    Fetch('https://hacker-news.firebaseio.com/v0/topstories.json')
    .then(stories => {
        const tasks = stories.map(id => {
            let storyURL = `https://hacker-news.firebaseio.com/v0/item/${id}.json`;
            return Fetch(storyURL);
        });

        return Promise.all(tasks);
    })
    .then(allStories => {
        return new Client({
            stories: allStories,
        });
    })
    .then((client) => {
        toggleLoader();
        return client;
    })
    .then((client) => {
        client.render();
    });
});

function toggleLoader(hide=true) {
    // We could use toggle here instead of add/remove
    if (hide) {
        document.querySelector('.loader').classList.add('hidden');
        document.querySelector('html').classList.remove('full-size');
        document.querySelector('#app').classList.remove('hidden');    
    } else {
        document.querySelector('.loader').classList.remove('hidden');
        document.querySelector('html').classList.add('full-size');
        document.querySelector('#app').classList.add('hidden');
    }
}











